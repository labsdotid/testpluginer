<?php

/**
 * Plugin Name:       Test Pluginer
 * Plugin URI:        https://www.salesloo.com
 * Description:       Powerful Membership & Affiliate Plugin For selling digital product with WordPress
 * Version:           0.14.1
 * Author:            Salesloo
 * Author URI:        https://www.salesloo.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       testplugin
 * Domain Path:       /languages
 */


defined('ABSPATH') || exit;


if (!class_exists('mishaUpdateChecker')) {

    class mishaUpdateChecker
    {

        public $plugin_slug;
        public $version;
        public $cache_key;
        public $cache_allowed;

        public function __construct()
        {

            $this->plugin_slug = plugin_basename(__DIR__);
            $this->version = '1.0';
            $this->cache_key = 'misha_custom_upd';
            $this->cache_allowed = false;

            add_filter('plugins_api', array($this, 'info'), 20, 3);
            add_filter('site_transient_update_plugins', array($this, 'update'));
            add_action('upgrader_process_complete', array($this, 'purge'), 10, 2);
            add_filter('upgrader_source_selection', array($this, 'fixDirectoryName'), 10, 3);
        }

        public function request()
        {

            // $remote = get_transient($this->cache_key);

            // if (false === $remote || !$this->cache_allowed) {

            //     $remote = wp_remote_get(
            //         'https://rudrastyh.com/wp-content/uploads/updater/info.json',
            //         array(
            //             'timeout' => 10,
            //             'headers' => array(
            //                 'Accept' => 'application/json'
            //             )
            //         )
            //     );

            //     if (
            //         is_wp_error($remote)
            //         || 200 !== wp_remote_retrieve_response_code($remote)
            //         || empty(wp_remote_retrieve_body($remote))
            //     ) {
            //         return false;
            //     }

            //     set_transient($this->cache_key, $remote, DAY_IN_SECONDS);
            // }

            // $remote = json_decode(wp_remote_retrieve_body($remote));

            $remote = new stdClass();
            $remote->name           = 'Test Pluginer';
            $remote->slug           = 'testpluginer';
            $remote->version        = '1.0.0';
            $remote->tested         = '5.8';
            $remote->requires       = '5.0';
            $remote->author         = 'Salesloo';
            $remote->author_profile = 'https://www.salesloo.com';
            $remote->download_url  = 'https://bitbucket.org/labsdotid/testpluginer/get/master.zip';
            $remote->requires_php   = '7.1';
            $remote->last_updated   = '2021-08-25 11:37:1';
            $remote->sections = (object)[
                'description' => 'Ini deskrpto',
                'changelog' => 'Ini Changelog',
                'installation' => 'Ini Install'
            ];

            return $remote;
        }


        function info($res, $action, $args)
        {

            // print_r( $action );
            // print_r( $args );

            // do nothing if you're not getting plugin information right now
            if ('plugin_information' !== $action) {
                return false;
            }

            // do nothing if it is not our plugin
            if ($this->plugin_slug !== $args->slug) {
                return false;
            }

            // get updates
            $remote = $this->request();

            if (!$remote) {
                return false;
            }

            $res = new stdClass();

            $res->name           = $remote->name;
            $res->slug           = $remote->slug;
            $res->version        = $remote->version;
            $res->tested         = $remote->tested;
            $res->requires       = $remote->requires;
            $res->author         = $remote->author;
            $res->author_profile = $remote->author_profile;
            $res->download_link  = $remote->download_url;
            $res->trunk          = $remote->download_url;
            $res->requires_php   = $remote->requires_php;
            $res->last_updated   = $remote->last_updated;

            $res->sections = array(
                'description' => $remote->sections->description,
                'installation' => $remote->sections->installation,
                'changelog' => $remote->sections->changelog
            );

            if (!empty($remote->banners)) {
                $res->banners = array(
                    'low' => $remote->banners->low,
                    'high' => $remote->banners->high
                );
            }

            return $res;
        }

        public function update($transient)
        {

            if (empty($transient->checked)) {
                return $transient;
            }

            $remote = $this->request();

            if (
                $remote
                && version_compare($this->version, $remote->version, '<')
                && version_compare($remote->requires, get_bloginfo('version'), '<')
                && version_compare($remote->requires_php, PHP_VERSION, '<')
            ) {
                $res = new stdClass();
                $res->slug = $this->plugin_slug;
                $res->plugin = plugin_basename(__FILE__); // misha-update-plugin/misha-update-plugin.php
                $res->new_version = $remote->version;
                $res->tested = $remote->tested;
                $res->package = $remote->download_url;

                $transient->response[$res->plugin] = $res;
            }

            return $transient;
        }

        public function purge($upgrader, $options)
        {

            if (
                $this->cache_allowed
                && 'update' === $options['action']
                && 'plugin' === $options['type']
            ) {
                // just clean the cache when new plugin version is installed
                delete_transient($this->cache_key);
            }
        }

        public function fixDirectoryName($source, $remoteSource, $upgrader)
        {
            global $wp_filesystem;
            /** @var WP_Filesystem_Base $wp_filesystem */

            //Basic sanity checks.
            if (!isset($source, $remoteSource, $upgrader, $upgrader->skin, $wp_filesystem)) {
                return $source;
            }

            //If WordPress is upgrading anything other than our plugin/theme, leave the directory name unchanged.
            if (!$this->is_being_upgraded($upgrader)) {
                return $source;
            }

            //Rename the source to match the existing directory.
            $correctedSource = trailingslashit($remoteSource) . $this->plugin_slug . '/';
            if ($source !== $correctedSource) {


                /** @var WP_Upgrader_Skin $upgrader ->skin */
                $upgrader->skin->feedback(sprintf(
                    'Renaming %s to %s&#8230;',
                    '<span class="code">' . basename($source) . '</span>',
                    '<span class="code">' . $this->plugin_slug . '</span>'
                ));

                if (
                    $wp_filesystem->move($source, $correctedSource, true)
                ) {
                    $upgrader->skin->feedback('Directory successfully renamed.');
                    return $correctedSource;
                } else {
                    return new WP_Error(
                        'puc-rename-failed',
                        'Unable to rename the update to match the existing directory.'
                    );
                }
            }

            return $source;
        }

        private function is_being_upgraded($upgrader)
        {
            if (!isset($upgrader, $upgrader->skin)) {
                return false;
            }

            $plugin_file = null;

            $skin = $upgrader->skin;
            if ($skin instanceof Plugin_Upgrader_Skin) {
                if (isset($skin->plugin) && is_string($skin->plugin) && ($skin->plugin !== '')) {
                    $plugin_file = $skin->plugin;
                }
            }

            if ($plugin_file == null) {
                return false;
            }

            if ($plugin_file != plugin_basename(__FILE__)) {
                return false;
            }

            return true;
        }
    }

    new mishaUpdateChecker();
}
